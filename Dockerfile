FROM alpine:3.10

ARG BUILD_DATE
ARG VCS_REF

LABEL maintainer="Andy Cungkrinx <andy.silva270114@gmail.com>" \
    architecture="amd64/x86_64" \
    mariadb-version="10.3" \
    alpine-version="3.10" \
    build="23-July-2020" \
    org.opencontainers.image.title="alpine-mariadb" \
    org.opencontainers.image.description="MariaDB Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="Andy Cungkrinx <andy.silva270114@gmail.com>" \
    org.opencontainers.image.vendor="Arsya Technology" \
    org.opencontainers.image.version="v1.0" \
    org.opencontainers.image.url="https://hub.docker.com/andycungkrinxxx/mariadb/" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE

RUN apk add --no-cache mariadb mariadb-client mariadb-server-utils pwgen tzdata; \
    cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime; \
    echo "Asia/Jakarta" >  /etc/timezone; \
    rm -f /var/cache/apk/*

ADD script/run.sh /scripts/run.sh
RUN mkdir /docker-entrypoint-initdb.d; \
    mkdir /scripts/pre-exec.d; \
    mkdir /scripts/pre-init.d; \
    mkdir /var/log/mysql; \
    chown -R mysql:mysql /var/log/mysql/; \
    chmod -R 755 /scripts
COPY config/my.cnf /etc/my.cnf
EXPOSE 3306

VOLUME ["/var/lib/mysql"]

ENTRYPOINT ["/scripts/run.sh"]
